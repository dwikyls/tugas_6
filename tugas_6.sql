# JUMLAH COMPLAINT SETIAP BULAN
SELECT MONTH(`date_received`) AS bulan, COUNT(complaint_id) AS jml
FROM complaints group by bulan;

# TAGS OLDER AMERICAN
SELECT * FROM complaints WHERE tags = 'Older American';

# JUMLAH TYPE COMPANY RESPONSE PER COMPANY
CREATE VIEW response AS (
SELECT company,
       CASE WHEN company_response_to_consumer = 'Closed' THEN company_response_to_consumer END AS closed,
       CASE WHEN company_response_to_consumer = 'Closed with explanation'
           THEN company_response_to_consumer END AS explanation,
       CASE WHEN company_response_to_consumer = 'Closed with monetary relief'
           THEN company_response_to_consumer END AS monetary
FROM complaints
                        );

SELECT company,
       COUNT(closed) AS closed,
       COUNT(explanation) AS explanation,
       COUNT(monetary) AS monetary
FROM response
GROUP BY company;
